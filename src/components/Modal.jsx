import React from "react";
import { Dialog, DialogActions, Paper } from "@mui/material";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import CloseIcon from "@mui/icons-material/Close";


const Modal = ({
  heading,
  children,
  onOpen,
  onClose,
  footer,
  width = "md",
}) => {
  return (
    <>
      <Dialog
        open={onOpen}
        fullWidth={true}
        maxWidth={width}
        onClose={(event, reason) => {
          if (reason !== "backdropClick" && reason !== "escapeKeyDown") {
            onClose();
          } else {
            return;
          }
        }}
        BackdropProps={{
          sx: {
            backgroundColor: "rgba(255, 255, 255, 1)", // Set the desired background color here
          },
        }}
      >
        <Paper
          style={{
            height: '95vh',
            width: "100%",
          }}
        >
          <Toolbar sx={{ justifyContent: "space-between", backgroundColor: "#1976d2", color: "white" }}>
            <span>&nbsp;</span>
            <DialogTitle sx={{ padding: 0 }}>{heading}</DialogTitle>
            <IconButton
              edge="end"
              color="inherit"
              onClick={onClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
          <DialogContent sx={{
            padding: "10px", height: "66vh",
            overflowX: "hidden",
            overflowY: "scroll"
          }}>{children} </DialogContent>
          <DialogActions sx={{
            position: 'absolute',
            bottom: "5px"
          }}>{footer}</DialogActions>
        </Paper>
      </Dialog>
    </>
  );
};

export default Modal;
