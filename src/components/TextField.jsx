import React from "react";
import { FormControl, InputLabel } from "@mui/material";
import TextField from "@mui/material/TextField";

const TextInput = ({
    name,
    label,
    value,
    onChange,
    error,
    helperText,
    type,
    disable,
    placeHolder,
    size
}) => {

    return (
        <FormControl fullWidth sx={{ my: 0.5 }} size="small">
            {/* {label && <InputLabel >{label}</InputLabel>} */}
            <TextField
                type={type}
                value={value}
                label={label}
                name={name}
                onChange={onChange}
                variant={"outlined"}
                error={error}
                size={size}
                placeholder={placeHolder}
                helperText={helperText}
                disabled={disable}
            />
        </FormControl>
    );
};

export default TextInput;