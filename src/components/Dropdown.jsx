import React from 'react';
import {
    FormControl,
    Select,
    MenuItem,
    FormHelperText,
    InputLabel,
} from '@mui/material';

const Dropdown = ({ options, name, label, error, helperText, value, onChange, disable, size, width = false }) => {

    return (
        <FormControl fullWidth size="small" sx={{ mt: 1, width: width ? width : "100%" }}>
            {label && <InputLabel >{label}</InputLabel>}
            <Select
                name={name}
                value={value}
                label={label}
                error={error}
                onChange={onChange}
                variant={"outlined"}
                disabled={disable}
                size={size}
            >
                {options ? options?.map((option, index) => (
                    <MenuItem key={index} value={option?.value}  >
                        {option?.label}
                    </MenuItem>
                )) : <MenuItem>No Options</MenuItem>}
            </Select>
            <FormHelperText>{helperText}</FormHelperText>
        </FormControl>
    );
};

export default Dropdown;