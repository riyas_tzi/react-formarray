import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";

const Datagrid = ({
  xRowId,
  xrows,
  xcolumns,
  xonrowClick,
}) => {

  return (
    <DataGrid
      sx={{
        height: '80vh',
        width: '80%',
        cursor: "pointer",
        boxShadow: 2,
        border: 2,
        borderRadius: 0,
        borderColor: "primary.light",
        "& .MuiDataGrid-cell:hover": {
          color: "primary.main",
        },
      }}
      rows={xrows}
      columns={xcolumns}
      // rowHeight={30}
      headerHeight={10}
      // initialState={{
      //   pagination: {
      //     paginationModel: {
      //       pageSize: 10,
      //     },
      //   },
      // }}
      // getRowClassName={(params) =>
      //   params.indexRelativeToCurrentPage % 2 === 0
      //     ? "mui-even-row"
      //     : "mui-odd-row"
      // }
      getRowId={xRowId}
      onCellClick={xonrowClick}
      pageSizeOptions={[5]}
    />
  );
};
export default Datagrid;
