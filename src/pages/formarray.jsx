import React, { useEffect, useState } from 'react'

import DataGrid from '../components/Datagrid';
import Modal from "../components/Modal";
import { Button, Grid, Stack } from '@mui/material';
import Dropdown from '../components/Dropdown';
import TextInput from '../components/TextField';

export default function Formarray() {

    const getRowId = (row) => row.id;
    const [open, setOpen] = useState(false)
    const [editId, setIsEdit] = useState(null)
    const [formArray, setFormarray] = useState([
        {
            name: '',
            email: '',
            city: '',
            state: '',
            country: ''
        }
    ])
    const [records, setRecords] = useState([])
    const columns = [
        {
            field: 'name', headerName: 'User Name', width: 160
        },
        {
            field: 'email', headerName: 'Email address', width: 250
        },
        {
            field: 'city', headerName: 'City',
        },
        {
            field: 'state', headerName: 'State',
        },
        {
            field: 'country', headerName: 'Country',
        },
        {
            headerName: 'Actions', width: 200,
            renderCell: (params) => (
                <>
                    <Button
                        variant="contained"
                        size="small"
                        style={{ marginLeft: 16 }}
                        tabIndex={params.hasFocus ? 0 : -1}
                        onClick={() => onEdit(params?.row?.id, params?.row)}
                    >
                        edit
                    </Button>
                    <Button
                        variant="contained"
                        size="small"
                        style={{ marginLeft: 16 }}
                        tabIndex={params.hasFocus ? 0 : -1}
                        onClick={() => onDelete(params?.row?.id)}
                    >
                        delete
                    </Button></>),
        },
    ];

    const countryList = [
        { label: "India", value: "india" },
        { label: "Russia", value: "russia" },
        { label: "China", value: "china" }
    ]

    const [errors, setErrors] = useState({});

    const validateForm = () => {
        let isValid = true;
        const validateField = (fieldName, errorMessage) => {
            const isFieldValid = formArray.every(obj => obj[fieldName].trim() !== '');
            setErrors(prev => ({ ...prev, [fieldName]: isFieldValid ? '' : errorMessage }));
            if (!isFieldValid) {
                isValid = false;
            }
        };
        validateField('name', 'name is required');
        validateField('email', 'email is required');
        validateField('city', 'city is required');
        validateField('state', 'state is required');
        validateField('country', 'country is required');

        return isValid;
    };

    const handleChange = (index, event) => {
        event.preventDefault();
        formArray[index][event?.target?.name] = event?.target?.value;
        setFormarray(() => {
            return [...formArray]
        });
    }
    const saveFormData = (event) => {
        event.preventDefault();
        if (!validateForm()) {
            return;
        } else {
            if (editId !== null && editId !== undefined) {
                setRecords(() => [...records])
                setFormarray(() => {
                    return [{
                        name: '',
                        email: '',
                        city: '',
                        state: '',
                        country: ''
                    }]
                })
                setIsEdit(null)
            } else {
                setRecords((prev) => {
                    return [...prev, ...formArray].map((obj, i) => ({ ...obj, id: i }))
                })
                setFormarray(() => {
                    return [{
                        name: '',
                        email: '',
                        city: '',
                        state: '',
                        country: ''
                    }]
                })
            }
            closeModal()
        }

    }
    const removeForm = (event, index) => {
        event.preventDefault();
        let removedForm = formArray.filter((val, i) => i !== index)
        setFormarray(() => {
            return [...removedForm]
        });
    }
    const addForm = (event) => {
        event.preventDefault();
        setFormarray((value) => {
            return [...value,
            {
                name: '',
                email: '',
                city: '',
                state: '',
                country: ''
            }
            ];
        })
    }
    const closeModal = (event) => {
        setOpen(false)
        setIsEdit(null)
        setFormarray(() => {
            return [{
                name: '',
                email: '',
                city: '',
                state: '',
                country: ''
            }]
        })
        setErrors({})
    }

    const onEdit = (id, object) => {
        setOpen(true)
        setFormarray(() => {
            return [object]
        })
        setIsEdit(() => id)
    }
    const onDelete = (id) => {
        let updatedrecords = records.filter((val, i) => val?.id !== id)
        setRecords(() => [...updatedrecords])
    }

    useEffect(() => {
        console.log("formData", formArray);
    }, [formArray])
    useEffect(() => {
        console.log("records", records);
    }, [records])


    return (
        <div>
            <Modal
                onOpen={open}
                onClose={closeModal}
                heading={"FormArray Example"}
                children={<>
                    <form >
                        {formArray.map((obj, index) => {
                            return (<div key={index} style={{ margin: "20px 0" }}>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={6}>
                                        <TextInput
                                            name={"name"}
                                            label={"User Name"}
                                            type={"text"}
                                            value={obj?.name}
                                            onChange={(event) => handleChange(index, event)}
                                            error={Boolean(errors?.name)}
                                            helperText={errors?.name}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextInput
                                            name={"email"}
                                            label={"Email address"}
                                            type={"email"}
                                            value={obj?.email}
                                            onChange={(event) => handleChange(index, event)}
                                            error={Boolean(errors?.email)}
                                            helperText={errors?.email}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6} md={4}>
                                        <TextInput
                                            name={"city"}
                                            label={"City"}
                                            type={"text"}
                                            value={obj?.city}
                                            onChange={(event) => handleChange(index, event)}
                                            error={Boolean(errors?.city)}
                                            helperText={errors?.city}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6} md={4}>
                                        <TextInput
                                            name={"state"}
                                            label={"State"}
                                            type={"text"}
                                            value={obj?.state}
                                            onChange={(event) => handleChange(index, event)}
                                            error={Boolean(errors?.state)}
                                            helperText={errors?.state}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={4}>
                                        <Dropdown
                                            name={"country"}
                                            label={"Country"}
                                            options={countryList}
                                            value={obj?.country}
                                            onChange={(event) => handleChange(index, event)}
                                            error={Boolean(errors?.country)}
                                            helperText={errors?.country}

                                        />
                                    </Grid>
                                </Grid>
                                <Button
                                    color="error"
                                    size={"small"}
                                    variant={"contained"}
                                    onClick={(e) => removeForm(e, index)}
                                >
                                    remove
                                </Button>
                            </div>)
                        })}
                    </form>
                </>}
                footer={<>
                    <Stack direction={"row"} spacing={2} justifyContent={"center"}>
                        <Button
                            type="submit"
                            size={"small"}
                            variant={"contained"}
                            onClick={saveFormData}
                        >
                            Save
                        </Button>
                        {!editId && <Button
                            size={"small"}
                            variant={"contained"}
                            onClick={(e) => addForm(e)}>
                            Add Form
                        </Button>}

                    </Stack>
                </>}
            />
            <div style={{ height: '95vh', width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                <Button onClick={() => setOpen(true)} variant={"contained"} sx={{ mb: 1 }} size={"small"} >Add User</Button>
                <DataGrid
                    xrows={records ?? []}
                    xcolumns={columns}
                    xRowId={getRowId}
                />
            </div>

        </div>
    )
}
