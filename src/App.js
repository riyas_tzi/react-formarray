import "./App.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Formarray from "./pages/formarray";

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';



function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Formarray />,
    },
  ]);

  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
